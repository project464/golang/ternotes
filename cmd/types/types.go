package types

type Note struct {
	ID          int64  `json:"id"`
	Tag         string `json:"tag"`
	Command     string `json:"command"`
	Description string `json:"description"`
}

type NoteCollection struct {
	Notes []Note `json:"items"`
}
